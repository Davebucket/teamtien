namespace KLM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TruckModel : DbContext
    {
        public TruckModel()
            : base("name=TruckModelConn")
        {
        }

        public virtual DbSet<MANPROJCTDATA> MANPROJCTDATA { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
