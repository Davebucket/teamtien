﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VINSALLINVALID.ascx.cs" Inherits="KLM.UserControls.VINSALLINVALID" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
   <div class="container-fluid">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="Content/GridEvents.js" defer></script>
        <div class="row">
            <p class="col-12 demo-content-title">All invalid VINS</p>
        </div>
        <div class="row">
            <div class="col-12">    
                <dx:BootstrapGridView runat="server" ID="vinGridView" DataSourceID="EntityServerModeDataSourceVIN" KeyFieldName="rownum" OnToolbarItemClick="vinGridView_ToolbarItemClick" OnCustomCallback="vinGridView_CustomCallback" ClientInstanceName="vinGridView" OnCustomJSProperties="vinGridView_CustomJSProperties">
                            <ClientSideEvents ToolbarItemClick="onToolbarItemClick" EndCallback="onBootstrapGridViewEndCallback" Init="onGridInit" />
                            <SettingsPager AlwaysShowPager="true" PageSizeItemSettings-Visible="true" PageSize="15"></SettingsPager>
                            <Settings ShowHeaderFilterButton="true" GridLines="Horizontal" />
                            <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                            <SettingsText Title="Filtering" />
                            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
                            <SettingsPager EnableAdaptivity="true">
                                <Summary Position="Right" />
                                <NextPageButton IconCssClass="demo-icon demo-icon-right" Text="" />
                                <PrevPageButton IconCssClass="demo-icon demo-icon-left" Text="" />
                            </SettingsPager>
                            <CssClasses Control="demo-gridview" HeaderRow="bg-light text-dark" Row="grid-row" Table="border-top-0" />
                            <Columns>
                                <dx:BootstrapGridViewCommandColumn ShowSelectCheckbox="True" ShowClearFilterButton="true" SelectAllCheckboxMode="Page" Width="6" />
                                <dx:BootstrapGridViewDataColumn AllowTextTruncationInAdaptiveMode="true" FieldName="rownum"  Width="6" HorizontalAlign="Left" Visible="true" />
                                <dx:BootstrapGridViewDataColumn FieldName="OEM">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="Assignor">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="VIN">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Country_of_purchasing__leasing_entity">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Country_of_seller___lessor">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Date_of_purchase_">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Acquisition_type">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Procedure">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                            </Columns>
                            <Toolbars>
                                <dx:BootstrapGridViewToolbar ShowInsidePanel="true">
                                    <SettingsAdaptivity EnableCollapseRootItemsToIcons="true" Enabled="true" />
                                    <Items>
<%--                                        <dx:BootstrapGridViewToolbarItem Command="New">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Edit">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Delete">
                                        </dx:BootstrapGridViewToolbarItem>--%>
                                        <dx:BootstrapGridViewToolbarItem Text="Export" IconCssClass="demo-icon demo-icon-export">
                                            <Items>
                                                <dx:BootstrapGridViewToolbarMenuItem Command="ExportToCsv" Text="Export to Excel (XLSX)"></dx:BootstrapGridViewToolbarMenuItem>
                                                <dx:BootstrapGridViewToolbarMenuItem BeginGroup="true" Name="CustomExportToXLS" Text="Custom Export to XLS(WYSIWYG)" Command="Custom" />
                                            </Items>
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="ShowCustomizationDialog">
                                        </dx:BootstrapGridViewToolbarItem>
                                    </Items>
                                </dx:BootstrapGridViewToolbar>
                            </Toolbars>
                            <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="DataAware" ExportSelectedRowsOnly="false"></SettingsExport>
                        </dx:BootstrapGridView>
            </div>
            <dx:EntityServerModeDataSource ID="EntityServerModeDataSourceVIN" ContextTypeName="KLM.InvalidALLVIN" TableName="vwALLInavildVINS" OnSelecting="EntityServerModeDataSourceVIN_Selecting" runat="server" />
            <dx:BootstrapPopupMenu ID="bootstrapPopupMenu" runat="server" ClientInstanceName="bootstrapPopupMenu">
                <ClientSideEvents ItemClick="onBootstrapPopupMenuClick" />
                <Items>
                   <dx:BootstrapMenuItem Text="Search with Decisiv" IconCssClass="glyphicon glyphicon-floppy-save" Name="Decisiv" />
<%--                    <dx:BootstrapMenuItem Text="Add" IconCssClass="glyphicon glyphicon-plus" Name="Insert" />
                    <dx:BootstrapMenuItem Name="Update" IconCssClass="glyphicon glyphicon-refresh" />
                    <dx:BootstrapMenuItem Name="Delete" IconCssClass="glyphicon glyphicon-remove" />
                    <dx:BootstrapMenuItem Name="FilterRow" IconCssClass="glyphicon glyphicon-filter" />--%>
<%--                    <dx:BootstrapMenuItem Name="SearchPanel" IconCssClass="glyphicon glyphicon-search" />--%>
   <%--                 <dx:BootstrapMenuItem Name="GroupPanel" IconCssClass="glyphicon glyphicon-compressed" />--%>
<%--                    <dx:BootstrapMenuItem Name="ID" />
                    <dx:BootstrapMenuItem Name="Name" />
                    <dx:BootstrapMenuItem Name="Type" />--%>
                </Items>
            </dx:BootstrapPopupMenu>            
        </div>
    </div>
