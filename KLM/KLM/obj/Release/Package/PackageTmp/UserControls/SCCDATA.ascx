﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SCCDATA.ascx.cs" Inherits="KLM.UserControls.SCCDATA" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>


   <div class="container-fluid">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="Content/GridEvents.js" defer></script>
        <div class="row">
            <p class="col-12 demo-content-title">SCC DATA</p>
        </div>
        <div class="row">
            <div class="col-12">
                <dx:BootstrapGridView runat="server" ID="klmGridView" DataSourceID="EntityServerModeDataSource" KeyFieldName="RowNum" OnToolbarItemClick="GridViewCustomToolbar_ToolbarItemClick">
                            <ClientSideEvents ToolbarItemClick="onToolbarItemClick" />
                            <SettingsPager AlwaysShowPager="true" PageSizeItemSettings-Visible="true" PageSize="15"></SettingsPager>
                            <Settings ShowHeaderFilterButton="true" GridLines="Horizontal" />
                            <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                            <SettingsText Title="Filtering" />
                            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
                            <SettingsPager EnableAdaptivity="true">
                                <Summary Position="Right" />
                                <NextPageButton IconCssClass="demo-icon demo-icon-right" Text="" />
                                <PrevPageButton IconCssClass="demo-icon demo-icon-left" Text="" />
                            </SettingsPager>
                            <CssClasses Control="demo-gridview" HeaderRow="bg-light text-dark" Row="text-nowrap" Table="border-top-0" />
                            <SettingsSearchPanel Visible="true" ShowApplyButton="true" />
                            <Columns>
                                <dx:BootstrapGridViewCommandColumn ShowSelectCheckbox="True" ShowClearFilterButton="true" SelectAllCheckboxMode="Page" />
                                <dx:BootstrapGridViewDataColumn AllowTextTruncationInAdaptiveMode="true" FieldName="RowNum" Caption="rownum"  Width="6" HorizontalAlign="Left" />
                                <dx:BootstrapGridViewDataColumn FieldName="forwardername">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="country_code_origin">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="country_code_dest">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="y">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="sourcefile">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                            </Columns>
                            <Toolbars>
                                <dx:BootstrapGridViewToolbar ShowInsidePanel="true">
                                    <SettingsAdaptivity EnableCollapseRootItemsToIcons="true" Enabled="true" />
                                    <Items>
<%--                                        <dx:BootstrapGridViewToolbarItem Command="New">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Edit">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Delete">
                                        </dx:BootstrapGridViewToolbarItem>--%>
                                        <dx:BootstrapGridViewToolbarItem Text="Export" IconCssClass="demo-icon demo-icon-export">
                                            <Items>
                                                <dx:BootstrapGridViewToolbarMenuItem Command="ExportToCsv"></dx:BootstrapGridViewToolbarMenuItem>
                                                <dx:BootstrapGridViewToolbarMenuItem BeginGroup="true" Name="CustomExportToXLS" Text="Custom Export to XLS(WYSIWYG)" Command="Custom" />
                                            </Items>
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="ShowCustomizationDialog">
                                        </dx:BootstrapGridViewToolbarItem>
                                    </Items>
                                </dx:BootstrapGridViewToolbar>
                            </Toolbars>
                            <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="DataAware" ExportSelectedRowsOnly="true"></SettingsExport>
                        </dx:BootstrapGridView>
            </div>  
            <dx:EntityServerModeDataSource ID="EntityServerModeDataSource" runat="server" ContextTypeName="KLM.KLMModel" TableName="AirCargoData" OnSelecting="EntityServerModeDataSource_Selecting"/>
        </div>
   </div>