<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderTitle.ascx.cs" Inherits="KLM.HeaderTitle" %>

<div class="demo-header-part demo-header-title">
    <span class="demo-icon demo-icon-logo"></span>
    <div>
        <h3><a href="#" target="_top">Stibbe</a></h3>
        <h4><a href="#" target="_top">Litigation data</a></h4>
    </div>
</div>