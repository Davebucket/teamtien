<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigation.ascx.cs" Inherits="KLM.Navigation" %>

<%--<a href="https://www.devexpress.com/products/try/" target="_blank" class="demo-try-now-link bg-primary text-white">Try it now</a>--%>
<dx:BootstrapTreeView runat="server">
    <CssClasses
        IconExpandNode="demo-icon demo-icon-col m-0"
        IconCollapseNode="demo-icon demo-icon-ex m-0"
        NodeList="demo-treeview-nodes m-0" Node="demo-treeview-node" Control="demo-treeview" />
    <Nodes>
        <dx:BootstrapTreeViewNode Text="Dashboard" NavigateUrl="~/Default.aspx"></dx:BootstrapTreeViewNode>
        <dx:BootstrapTreeViewNode Text="Projects" Expanded="true">
            <Nodes>
<%--                <dx:BootstrapTreeViewNode Text="Tasks data table" NavigateUrl="~/TasksDataTable.aspx"></dx:BootstrapTreeViewNode>--%>
                <dx:BootstrapTreeViewNode Text="SCC data" NavigateUrl="~/AirCargo.aspx"></dx:BootstrapTreeViewNode>
                <dx:BootstrapTreeViewNode Text="Trucks data" NavigateUrl="~/TruckData.aspx" ToolTip="Batch1"></dx:BootstrapTreeViewNode>
<%--                <dx:BootstrapTreeViewNode Text="Event scheduling" NavigateUrl="~/EventScheduling.aspx"></dx:BootstrapTreeViewNode>--%>
            </Nodes>
        </dx:BootstrapTreeViewNode>
        <dx:BootstrapTreeViewNode Text="Extra Reports" Expanded="true">
            <Nodes>
                <dx:BootstrapTreeViewNode Text="Trucks double VINS" ToolTip="shows list of valid vins with double occurrence " NavigateUrl="~/VINSView.aspx?VIEWTOLOAD=DOUBLE"></dx:BootstrapTreeViewNode>
                <dx:BootstrapTreeViewNode Text="Trucks double invalid VINS" ToolTip="shows list of invalid vins with double occurrence" NavigateUrl="~/VINSView.aspx?VIEWTOLOAD=INVALID"></dx:BootstrapTreeViewNode>
                <dx:BootstrapTreeViewNode Text="Trucks all invalid VINS" ToolTip="shows list of all invalid vins" NavigateUrl="~/VINSView.aspx?VIEWTOLOAD=ALLINVALID"></dx:BootstrapTreeViewNode>            
            </Nodes>
        </dx:BootstrapTreeViewNode>
        <dx:BootstrapTreeViewNode Text="Tools" Expanded="true">
            <Nodes>
                <dx:BootstrapTreeViewNode Text="Statistics" NavigateUrl="~/Statistics.aspx" Visible="true"></dx:BootstrapTreeViewNode>
            </Nodes>
        </dx:BootstrapTreeViewNode>
<%--        <dx:BootstrapTreeViewNode Text="Documentation" NavigateUrl="https://docs.devexpress.com/AspNetBootstrap/118796/getting-started" Target="_blank"></dx:BootstrapTreeViewNode>--%>
    </Nodes>
</dx:BootstrapTreeView>