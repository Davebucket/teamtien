using DevExpress.Web.Bootstrap;
using System;
using KLM;
using DevExpress.XtraPrinting;
using DevExpress.Export;

namespace KLM {
    public partial class Default : System.Web.UI.Page {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Session["AccessDeniedAuthorize"].ToString().Equals("1"))
            {

                Response.Clear();
                Response.Redirect("~/AccessDenied.html");

            }
        }
        protected void CardViewControl_CustomCallback(object sender, DevExpress.Web.ASPxCardViewCustomCallbackEventArgs e) {
            int newPageSize = Int32.Parse(e.Parameters);
            //CardViewControl.SettingsPager.ItemsPerPage = newPageSize;
            //CardViewControl.DataBind();
        }

        protected void BootstrapScheduler1_Init(object sender, EventArgs e) {
            var scheduler = (BootstrapScheduler)sender;
            scheduler.Storage.Appointments.Labels.Clear();
            foreach(SchedulerLabel label in SchedulerLabelsHelper.GetItems())
                scheduler.Storage.Appointments.Labels.Add(label.Id, label.Name, label.BackgroundCssClass, label.TextCssClass);
        }

        //protected void EntityServerModeDataSource_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        //{
        //    e.KeyExpression = "RowNum";
        //}

        //protected void GridViewCustomToolbar_ToolbarItemClick(object source, BootstrapGridViewToolbarItemClickEventArgs e)
        //{
        //    switch (e.Item.Command.ToString())
        //    {
        //        case "Custom":
        //            klmGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
        //            break;
        //        case "ExportToCsv":
        //            klmGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.DataAware });
        //            break;
        //        default:
        //            break;
        //    }
        //}

        //protected void ButtonPDF1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportPdfToResponse();
        //}
        //protected void ButtonXLS1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
        //}
        //protected void ButtonXLSX1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        //}
        //protected void ButtonCSV1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportCsvToResponse(new CsvExportOptionsEx() { ExportType = ExportType.WYSIWYG });
        //}
        //protected void ButtonDOCX1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportDocxToResponse();
        //}
        //protected void ButtonRTF1_Click(object sender, EventArgs e)
        //{
        //    klmGridView.ExportRtfToResponse();
        //}
    }
}