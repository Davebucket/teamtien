namespace KLM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwALLInavildVINS
    {
        [Key]
        public int rownum { get; set; }

        public string OEM { get; set; }

        public string Assignor { get; set; }

        public string VIN { get; set; }

        [Column("Country of purchasing/ leasing entity")]
        public string Country_of_purchasing__leasing_entity { get; set; }

        [Column("Country of seller/  lessor")]
        public string Country_of_seller___lessor { get; set; }

        [Column("Date of purchase ")]
        public string Date_of_purchase_ { get; set; }

        [Column("Acquisition type")]
        public string Acquisition_type { get; set; }

        public string Procedure { get; set; }
    }
}
