﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="VINSView.aspx.cs" Inherits="KLM.VINSView" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<asp:Content runat="server" ContentPlaceHolderID="mainContent">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="Content/GridEvents.js" defer></script>


    <div class="container-fluid">
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="100%" ClientInstanceName="cp" OnCallback="ASPxCallbackPanel1_Callback" EnableCallbackAnimation="true">
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <div class="page-content">
                        <div class="page-header position-relative">
							<small>
                                <i class="icon-double-angle-right"></i>

                            </small>
                        </div>
                        <div class="row-fluid">
                            <div id="div1" runat="server" style="width: 100%; height: 100%;">

                            </div>
                        </div>
                    </div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
</asp:Content>