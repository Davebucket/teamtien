﻿using DevExpress.Web.Bootstrap;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KLM
{
    public partial class SearchResults : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Session["AccessDeniedAuthorize"].ToString().Equals("1"))
            {

                Response.Clear();
                Response.Redirect("~/AccessDenied.html");

            }
            if (!IsPostBack && !IsCallback)
            {
                //Create columns on the first load.
                BootstrapGridViewDataColumn DOCNAME = new BootstrapGridViewDataColumn();
                DOCNAME.FieldName = "DOCNAME";
                DOCNAME.Caption = "Documents";
                vinBootstrapGridView.Columns.Add(DOCNAME);

                BootstrapGridViewDataColumn VIN = new BootstrapGridViewDataColumn();
                VIN.FieldName = "VIN";
                vinBootstrapGridView.Columns.Add(VIN);

                /*
                BootstrapGridViewDataColumn Location = new BootstrapGridViewDataColumn();
                Location.FieldName = "Location";
                vinBootstrapGridView.Columns.Add(Location);
                */


            }

            if (Session["Table"] != null)
            {
                if (!IsPostBack)
                    vinBootstrapGridView.DataBind();
            }
        }

        protected void vinBootstrapGridView_DataBinding(object sender, EventArgs e)
        {
            vinBootstrapGridView.DataSource = GetTable();
        }

        DataTable GetTable()
        {
            //You can store a DataTable in the session state
            DataTable table = Session["Table"] as DataTable;
            if (table == null)
            {
                table = new DataTable();
                table.Columns.Add("docname", typeof(int));
                table.Columns.Add("vin", typeof(String));
                for (int n = 0; n < 100; n++)
                {
                    table.Rows.Add(n, "row" + n.ToString());
                }
                Session["Table"] = table;
            }

            return table;
        }

        protected void vinBootstrapGridView_CustomColumnDisplayText(object sender, BootstrapGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "DOCNAME")
            {
                var location = e.GetFieldValue("Location");
                BootstrapGridView grid = sender as BootstrapGridView;
                e.EncodeHtml = false;
                //e.DisplayText = "<a href='" + e.Value + "'>" + Convert.ToString(grid.GetRowValues(e.VisibleIndex, "ProductName")) + "</a>";
                //e.DisplayText = "<a href='" + e.Value + "'>" + "Open Location" + "</a>";
                e.DisplayText = "<a title=\"click to open this file!\" href=\"#\" OnClick=\"Open('" + System.Net.WebUtility.UrlEncode(location.ToString()) +"')\">" + e.Value+ "</a>";

            }

        }


        [WebMethod]
        public static string OpenServerFile(string url)
        {
            url = System.Net.WebUtility.UrlDecode(url);

            if (url.Contains("&"))
            {
                url = url.Replace("&","%26");
            }


            var uri = new Uri(url); // Here I get the error
            //string url2 = uri.AbsoluteUri;
            //var fName = Path.GetFullPath(uri.LocalPath);
            //var fileInfo = new FileInfo(fName);

            //var response = HttpContext.Current.Response;

            //response.Clear();
            //response.ClearContent();
            //response.ClearHeaders();

            //response.Buffer = true;

            //response.AddHeader("Content-Description", "attachment;filename=" + fileInfo.FullName);
            //response.WriteFile(fileInfo.FullName);
            //response.End();
            return url;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["VIEWTOLOAD"] != null)
            {
                if (Request.QueryString["VIEWTOLOAD"].Equals("TRUCKSVIN"))
                {
                    Response.Redirect("TruckData.aspx");

                }
                else
                {
                    Response.Redirect(string.Format("VINSView.aspx?VIEWTOLOAD={0}", Request.QueryString["VIEWTOLOAD"]));
                }
            }
        }
    }
}