namespace KLM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class InvalidVIN : DbContext
    {
        public InvalidVIN()
            : base("name=InvalidVIN")
        {
        }

        public virtual DbSet<vwInavildVINS> vwInavildVINS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
