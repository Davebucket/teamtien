namespace KLM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class KLMModel : DbContext
    {
        public KLMModel()
            : base("name=KLMModels")
        {
        }

        public virtual DbSet<AirCargoData> AirCargoData { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
