﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="Statistics.aspx.cs" Inherits="KLM.Statistics" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<%@ Register Src="~/UserControls/Statistics.ascx" TagPrefix="demo" TagName="STATContainer" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" src="Content/dashboard.js" defer></script>
    <script type="text/javascript" src="Content/GridEvents.js" defer></script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="mainContent">
        <div class="container-fluid">
            <div class="row mb-4">
                <demo:STATContainer ID="STAT" runat="server"/>
            </div>
        </div>
</asp:Content>
