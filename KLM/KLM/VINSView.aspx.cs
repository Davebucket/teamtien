﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KLM
{
    public partial class VINSView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Session["AccessDeniedAuthorize"].ToString().Equals("1"))
            {

                Response.Clear();
                Response.Redirect("~/AccessDenied.html");

            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            LoadUserControl();
        }

        protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {

        }

        protected void LoadUserControl()
        {
            if (Request.QueryString["VIEWTOLOAD"] == "DOUBLE")
            {
                UserControl uc = (UserControl)Page.LoadControl("~/UserControls//VINS.ascx");
                uc.ID = "ucVIN";
                div1.Controls.Add(uc);
            }
            else if (Request.QueryString["VIEWTOLOAD"] == "INVALID")
            {
                UserControl uc = (UserControl)Page.LoadControl("~/UserControls//VINSINVALID.ascx");
                uc.ID = "ucVININVALID";
                div1.Controls.Add(uc);
            }
            else if (Request.QueryString["VIEWTOLOAD"] == "ALLINVALID")
            {
                UserControl uc = (UserControl)Page.LoadControl("~/UserControls//VINSALLINVALID.ascx");
                uc.ID = "ucVINALLINVALID";
                div1.Controls.Add(uc);
            }
        }
    }
}