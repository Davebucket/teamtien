namespace KLM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DoubleVIN : DbContext
    {
        public DoubleVIN()
            : base("name=DoubleVIN")
        {
        }

        public virtual DbSet<DoubleVINNUmmers> DoubleVINNUmmers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
