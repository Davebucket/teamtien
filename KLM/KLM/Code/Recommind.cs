﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace KLM
{
    public static class Recommind
    {
        public static WebClient ConnectToRecommind()
        {
            var webClient = new WebClient();
            webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; " +
                                              "Windows NT 5.2; .NET CLR 1.0.3705;)");
            webClient.Encoding = Encoding.UTF8;
            webClient.Credentials = new NetworkCredential("admin", "*knowledge$");

            return webClient;
        }
    }

    public class TRUKS
    {
        public string docname { get; set; }
        public string rm_location { get; set; }
        public string vin { get; set; }
    }

    public class Status
    {
        public bool successful { get; set; }
        public string backendStatus { get; set; }
        public int httpStatus { get; set; }
        public object errorMessage { get; set; }
    }

    public class Field
    {
        public string id { get; set; }
        public string value { get; set; }
        public object valueObject { get; set; }
    }

    public class Result
    {
        public int rank { get; set; }
        public double relevance { get; set; }
        public string id { get; set; }
        public string uniqueField { get; set; }
        public List<Field> fields { get; set; }
        public List<object> folderSets { get; set; }
        public object body { get; set; }
        public int page { get; set; }
        public int pageCount { get; set; }
    }

    public class RootObject
    {
        public Status status { get; set; }
        public int numberResults { get; set; }
        public List<Result> results { get; set; }
    }
}