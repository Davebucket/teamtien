using System;
using System.Linq;

namespace KLM {

    public interface IWidget {
        string Title { get; set; }
    }
}