﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="SearchResults.aspx.cs" Inherits="KLM.SearchResults" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<asp:Content runat="server" ContentPlaceHolderID="mainContent">
    <script type="text/javascript" src="Content/GridEvents.js" defer></script>
    <div class="container-fluid">
        <div class="row">
            <p class="col-12 demo-content-title">Your Search Results</p>
        </div>
                <dx:BootstrapButton runat="server" Text="Back to VIN's" ID="btnBack" OnClick="btnBack_Click" ToolTip="back to previous vins view" CssClasses-Control="bg-info border-info text-white">
                    <CssClasses Icon="fa fa-mars-double" />
                </dx:BootstrapButton> <br/><br />
        <div class="row">
            <div class="col-lg-6">
                <dx:BootstrapGridView ID="vinBootstrapGridView" runat="server" AutoGenerateColumns="false" OnDataBinding="vinBootstrapGridView_DataBinding" OnCustomColumnDisplayText="vinBootstrapGridView_CustomColumnDisplayText">

                </dx:BootstrapGridView>
            </div>
        </div>
    </div>
    
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
</asp:Content>
