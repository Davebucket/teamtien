﻿using DevExpress.Export;
using DevExpress.Web.Bootstrap;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KLM.UserControls
{
    public partial class SCCDATA : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void EntityServerModeDataSource_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            e.KeyExpression = "RowNum";
        }

        protected void GridViewCustomToolbar_ToolbarItemClick(object source, BootstrapGridViewToolbarItemClickEventArgs e)
        {
            switch (e.Item.Command.ToString())
            {
                case "Custom":
                    klmGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    break;
                case "ExportToCsv":
                    klmGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.DataAware });
                    break;
                default:
                    break;
            }
        }
    }
}