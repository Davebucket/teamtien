﻿using DevExpress.Export;
using DevExpress.Web.Bootstrap;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KLM
{
    public partial class VINS : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void vinGridView_ToolbarItemClick(object sender, BootstrapGridViewToolbarItemClickEventArgs e)
        {
            switch (e.Item.Command.ToString())
            {
                case "Custom":
                    vinGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
                    break;
                case "ExportToCsv":
                    XlsxExportOptionsEx exportOptions = new XlsxExportOptionsEx();
                    exportOptions.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    exportOptions.ExportType = DevExpress.Export.ExportType.DataAware;
                    vinGridView.ExportXlsxToResponse(exportOptions);
                    //vinGridView.ExportXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.DataAware });
                    //vinGridView.ExportXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.DataAware });
                    break;
                default:
                    break;
            }
        }

        protected void EntityServerModeDataSourceVIN_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            e.KeyExpression = "rownum";
        }

        protected void vinGridView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            vinGridView.JSProperties["cpCallbackAction"] = e.Parameters;
            string[] parameters = e.Parameters.Split('|');

            switch (parameters[0])
            {
                case "FilterRow":
                    {
                        vinGridView.Settings.ShowFilterRow = vinGridView.Settings.ShowFilterRow ? false : true;
                        vinGridView.JSProperties["cpIsFilterRowVisible"] = vinGridView.Settings.ShowFilterRow;
                    }
                    break;
                case "SearchPanel":
                    {
                        vinGridView.SettingsSearchPanel.Visible = vinGridView.SettingsSearchPanel.Visible ? false : true;
                        vinGridView.JSProperties["cpIsSearchPanelVisible"] = vinGridView.SettingsSearchPanel.Visible;
                    }
                    break;
                case "GroupPanel":
                    {
                        vinGridView.Settings.ShowGroupPanel = vinGridView.Settings.ShowGroupPanel ? false : true;
                        vinGridView.JSProperties["cpIsGroupPanelVisible"] = vinGridView.Settings.ShowGroupPanel;
                    }
                    break;
                case "Decisiv":
                    {
                        //http://asdrm02.nl.stibbe.int/searchWebApi/projects/Meta_Engine_Project_Benelux_/collections/documents/records?fields=doc_name%2Cdoc_cat_project&query=XLRAE456FOL337758
                        string server = "asdrm02.nl.stibbe.int";
                        string queryText = parameters[1];
                        string query = string.Format("http://{0}/searchWebApi/projects/Meta_Engine_Project_Benelux_/collections/documents/records?fields=doc_name%2Crm_location&query={1}"
                            , server, queryText);

                        var webClient = Recommind.ConnectToRecommind();
                        var json = webClient.DownloadString(query);

                        RootObject r = JsonConvert.DeserializeObject<RootObject>(json);

                        List<TRUKS> trucks = new List<TRUKS>();

                        foreach (var item in r.results)
                        {
                            TRUKS t = new TRUKS();
                            t.docname = item.fields[0].value.ToString();
                            t.vin = parameters[1];
                            t.rm_location = item.fields[1].value.ToString().Replace("\\", @"\").Replace("E:\\data", "\\\\ASDRM02") + "\\" + t.docname;
                            //item.fields[1].value.ToString();
                            trucks.Add(t);
                        }

                        Session["TRUCKS"] = trucks;

                        //You can store a DataTable in the session state
                        DataTable table = new DataTable();

                        table = new DataTable();
                        table.Columns.Add("DOCNAME", typeof(String));
                        table.Columns.Add("VIN", typeof(String));
                        table.Columns.Add("Location", typeof(String));
                        foreach (TRUKS t in trucks)
                        {
                            table.Rows.Add(t.docname, t.vin, t.rm_location);
                        }
                        Session["Table"] = table;

                        if (Page.IsCallback)
                            DevExpress.Web.ASPxWebControl.RedirectOnCallback(string.Format("~/SearchResults.aspx?vin={0}&VIEWTOLOAD=DOUBLE", Session["Table"]));
                        else
                            Response.Redirect("~/SearchResults.aspx");

                    }
                    break;
            }
        }

        //https://www.devexpress.com/Support/Center/Example/Details/E168/bind-a-grid-to-a-datatable-via-code
        protected void vinGridView_CustomJSProperties(object sender, DevExpress.Web.ASPxGridViewClientJSPropertiesEventArgs e)
        {
            var startIndex = vinGridView.VisibleStartIndex;

            var endIndex = (vinGridView.PageIndex + 1) == vinGridView.PageCount ? vinGridView.VisibleRowCount : vinGridView.VisibleStartIndex + vinGridView.SettingsPager.PageSize;

            Dictionary<int, string> values = new Dictionary<int, string>();

            for (int i = startIndex; i < endIndex; i++)
            {
                object rowValues = vinGridView.GetRowValues(i, "VIN");

                values.Add(i, rowValues.ToString());
            }

            e.Properties.Add("cpVINS", values);
        }

        [WebMethod]
        public static List<TRUKS> SearchDecisiv(string vin)
        {
            string server = "asdrm02.nl.stibbe.int";
            string queryText = vin;
            string query = string.Format("http://{0}/searchWebApi/projects/Meta_Engine_Project_Benelux_/collections/documents/records?fields=doc_name&query={1}"
                , server, queryText);

            var webClient = Recommind.ConnectToRecommind();
            var json = webClient.DownloadString(query);

            RootObject r = JsonConvert.DeserializeObject<RootObject>(json);
            List<TRUKS> trucks = new List<TRUKS>();

            foreach (var item in r.results)
            {
                TRUKS t = new TRUKS();
                t.docname = item.fields[0].value.ToString();
            }
            return trucks;
        }
    }
}