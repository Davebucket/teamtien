﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KLM.UserControls
{
    public partial class Statistics : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //https://www.devexpress.com/Support/Center/Question/Details/T507580/how-to-browse-a-folder-and-get-its-path
        //http://jsfiddle.net/taditdash/9Fh4c/
        protected void CardViewControl_CustomCallback(object sender, DevExpress.Web.ASPxCardViewCustomCallbackEventArgs e)
        {
            int newPageSize = Int32.Parse(e.Parameters);
            CardViewControl.SettingsPager.ItemsPerPage = newPageSize;
            CardViewControl.DataBind();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            StatusLabel.Text = string.Empty;
            TotalLabel.Text = string.Empty;

            int count = 0;
            int totalCount = 0;

            if (FileUploadControl.HasFile)
            {
                try
                {
                    foreach (HttpPostedFile postedFile in FileUploadControl.PostedFiles)
                    {
                        Stream s = postedFile.InputStream;
                        count = GetExcelNumbersRecords(s);
                        totalCount = totalCount + count;
                        StatusLabel.Text += "<br/>" + count + " rows found in " + postedFile.FileName + "<br/>";
                    }

                    TotalLabel.Text = "Total of: " + totalCount.ToString() + " rows in selected files.";
                }
                catch (Exception ex)
                {

                    StatusLabel.Text = ex.Message;
                }    
            }
        }

        private int GetCountsFromPath()
        {

            int count = 0;
            int totalCount = 0;

            try
            {
                string path = Path.GetDirectoryName(FileUploadControl.PostedFile.FileName);

                Aspose.Cells.License license = new Aspose.Cells.License();
                license.SetLicense("Aspose.Total.lic");
                string[] filePaths = Directory.GetFiles(path);

                foreach (var file in filePaths)
                {
                    count = GetExcelNumbersRecords(file);
                    totalCount = totalCount + count;
                }

                StatusLabel.Text = "Total of: " + totalCount + " rows found in all Excel files!";
            }
            catch (Exception ex)
            {
                StatusLabel.Text = ex.Message;
            }

            return totalCount;
        }


        static int GetExcelNumbersRecords(string path)

        {

            Workbook wb = new Workbook(path);

            int rowcount = 0;



            foreach (Worksheet sheet in wb.Worksheets)

            {

                //gives the columns count, since the columns with standard

                int colcount = sheet.Cells.Columns.Count;

                // //gives the rows count, since all the rows with standard (default)

                rowcount = sheet.Cells.Rows.Count;

            }

            Console.WriteLine(rowcount + " rows in " + path);

            return rowcount;

        }

        static int GetExcelNumbersRecords(Stream s)
        {
            Workbook wb = new Workbook(s);

            int rowcount = 0;

            foreach (Worksheet sheet in wb.Worksheets)

            {

                //gives the columns count, since the columns with standard

                int colcount = sheet.Cells.Columns.Count;

                // //gives the rows count, since all the rows with standard (default)

                rowcount = sheet.Cells.Rows.Count;

            }

            return rowcount;
        }
    }
}