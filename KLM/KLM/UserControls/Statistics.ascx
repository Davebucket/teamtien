﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Statistics.ascx.cs" Inherits="KLM.UserControls.Statistics" %>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>

<script>
    function openfileDialog() {
        $("#fileLoader").click();
    }
    function getPath() {
        var path = fileManager.GetCurrentFolderPath('>', true);
        alert(path);
    }

    function savePath(s, e) {
        var path = s.GetCurrentFolderPath('>', true);
        var selectedFiles = s.GetSelectedItems();
        alert(selectedFiles);
        alert(path);
    }
</script>
<style>
.btnHeight, input, optgroup, select, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: 18px;
}
</style>
    <script type="text/javascript" src="Content/dashboard.js" defer></script>
    <div class="container-fluid">
        <div class="row">
            <p class="col-12 demo-content-title">Excel Statictics</p>
        </div>
    </div> <br />
    <div class="container-fluid" style="background-color:white; margin-right:200px;">
<%--        <div class="row">
            <p class="col-12 demo-content-title">
                <input type="file" id="fileLoader" name="files" title="Load File" />
                <input type="button" id="btnOpenFileDialog" value = "Click Me !!!" onclick="openfileDialog();" />
            </p>
        </div>--%>
      <div class="row">
            <p class="col-12 demo-content-title">
                <asp:FileUpload ID="FileUploadControl" runat="server" AllowMultiple="true" />
                <asp:Button runat="server" id="UploadButton" text="Calculate" OnClick="UploadButton_Click" CssClass="btnHeight" />
                <br /><br /> 
            </p>
      </div>
        <asp:Label runat="server" id="StatusLabel" text="Status: " /> <br />
        <asp:Label runat="server" id="TotalLabel" text="Total: " />

        <div class="row mb-4">
            <div class="col-12">
                <dx:BootstrapCardView ID="CardViewControl" ClientInstanceName="CardViewControl" runat="server" Visible="false"
                     DataSourceID="DashboardCardsDataSource" OnCustomCallback="CardViewControl_CustomCallback">
                    <CssClasses Control="shadow rounded text-white demo-card-view demo-widget-area bg-primary"
                        Content="bg-primary border-0"
                        Card="bg-primary demo-card-view-item" PanelBodyBottom="bg-primary" />
                    <CssClassesPager PageNumber="text-white" />
                    <SettingsPager ItemsPerPage="1" EnableAdaptivity="true">
                        <Summary Position="Right" />
                        <NextPageButton IconCssClass="demo-icon demo-icon-right" Text="" />
                        <PrevPageButton IconCssClass="demo-icon demo-icon-left" Text="" />
                    </SettingsPager>
                    <SettingsLayout CardColSpanXs="12" CardColSpanSm="12" CardColSpanMd="6" CardColSpanLg="4" CardColSpanXl="3" />
                    <Templates>
                        <Card>
                            <div class="d-flex">
                                <div class="p-2 mr-1">
                                    <span class="<%# Eval("IconName") %>"></span>
                                </div>
                                <div>
                                    <div class="text-nowrap" style="font-size: 2rem;">
                                        <span><%# Eval("Title") %>:</span>
                                        <span class="font-weight-bold ml-2"><%# Eval("ValueString") %></span>
                                    </div>
                                    <div class="font-weight-light">
                                        <p><%# Eval("Summary") %></p>
                                        <div style="font-size: 0.8rem;"><%# Eval("Category") %></div>
                                    </div>
                                </div>
                            </div>
                        </Card>
                    </Templates>
                </dx:BootstrapCardView>
            </div>
        </div>

    <asp:ObjectDataSource ID="DashboardCardsDataSource" runat="server"
        DataObjectTypeName="KLM.DashboardCard"
        TypeName="KLM.DataProvider"
        SelectMethod="GetDashboardCards"></asp:ObjectDataSource>
    </div>

