﻿function onToolbarItemClick(s, e) {
    switch (e.item.name) {
        case "CustomExportToXLS":
            e.processOnServer = true;
            e.usePostBack = true;
            break;
    }
}


var itemIndex;
var selectedVIN;
var gridViewElementsVisibility = {
    FilterRow: false,
    SearchPanel: false,
    GroupPanel: false,
    ID: true,
    Name: true,
    Type: true
}

function setMenuElementEnabled(menuElementName, isMenuElemenVisibleName) {
    gridViewElementsVisibility[menuElementName] = vinGridView[isMenuElemenVisibleName]
    vinGridView[isMenuElemenVisibleName] = null;
}

function onBootstrapGridViewEndCallback(s, e) {
    setMenuElementEnabled(s["cpCallbackAction"], "cpIs" + s["cpCallbackAction"] + "Visible");
    s["cpCallbackAction"] = null;
}

function onGridInit(s, e) {

    s.GetMainElement().addEventListener("contextmenu", function (evt) {
        var $sourceElement = $(ASPxClientUtils.GetEventSource(evt));

        ASPxClientUtils.PreventEvent(evt);
        updateMenuItem = bootstrapPopupMenu.GetItemByName("Update");
        //updateMenuItem.SetText("Edit");
        deleteMenuItem = bootstrapPopupMenu.GetItemByName("Delete");
        //deleteMenuItem.SetText("Remove");


        //".grid-row" is the css class defined in BootstrapGridView Classes Control, Row! Otherwise lenght would be 0! See line below.
        // <CssClasses Control="demo-gridview" HeaderRow="bg-light text-dark" Row="grid-row" Table="border-top-0" />
        var $gridRow = $sourceElement.parents(".grid-row");

        if ($gridRow.length) {
            itemIndex = s.GetPageIndex() * s.pageRowSize + $gridRow.index() - 1;
            var rowKey = s.GetRowKey(itemIndex);
            var vin = s.cpVINS[itemIndex];
            selectedVIN = vin;
            //updateMenuItem.SetText(updateMenuItem.GetText() + " " + rowKey);
            //deleteMenuItem.SetText(deleteMenuItem.GetText() + " " + rowKey);
            //updateMenuItem.SetEnabled(true);
            //deleteMenuItem.SetEnabled(true);
        }
        else {
            //updateMenuItem.SetEnabled(false);
            //deleteMenuItem.SetEnabled(false);
        }

        for (var i = 3; i < bootstrapPopupMenu.GetItemCount(); i++)
            setMenuItemText(bootstrapPopupMenu.GetItem(i).name);

        bootstrapPopupMenu.ShowAtPos(evt.clientX, evt.clientY);
    });
}

function onBootstrapPopupMenuClick(s, e) {
    var itemName = e.item.name;

    switch (itemName) {
        case "Insert":
            vinGridView.AddNewRow();
            break;
        case "Update":
            vinGridView.StartEditRow(itemIndex);
            break;
        case "Delete":
            vinGridView.DeleteRow(itemIndex);
            break;
        default:
            vinGridView.PerformCallback(itemName + "|" + selectedVIN);
            break;
    }
}




function Open(value) {
    var Tvalue = value;

    $.ajax({
        type: "POST",
        url: "SearchResults.aspx/OpenServerFile",
        data: "{url: '" + value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "text json",
        success: function(result) {
            var r = result.d;
            window.open("DownloadFile.ashx?path=" + r);
        },
        error: function (xhr, httpStatusMessage, customErrorMessage) {
            if (xhr.status === 410) {
                alert(customErrorMessage);
            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}