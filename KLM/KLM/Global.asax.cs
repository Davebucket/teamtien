﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using VookusDatabaseLibrary;

namespace KLM
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {


        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Database db = new Database("security");
            string user = User.Identity.Name;
            string allowedUser = db.ExecuteScalarAsString(string.Format("select Count(samaccountname) from dbo.person where samaccountname = '{0}'", user));

            Session["AccessDeniedAuthorize"] = allowedUser;

            if (!Session["AccessDeniedAuthorize"].ToString().Equals("1"))
            {

                Response.Clear();
                Response.Redirect("~/AccessDenied.html");

            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}