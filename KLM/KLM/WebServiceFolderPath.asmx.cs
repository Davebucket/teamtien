﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace KLM
{
    /// <summary>
    /// Summary description for WebServiceFolderPath
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceFolderPath : System.Web.Services.WebService
    {

        [WebMethod]
        public string FullPath(string fileName)
        {
            string path = Path.GetDirectoryName(fileName);


            return path;
        }
    }
}
