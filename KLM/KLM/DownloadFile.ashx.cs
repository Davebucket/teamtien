﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KLM
{
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string path = string.Empty;        

            path = HttpContext.Current.Request["path"]; 

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();

            var fileInfo = new System.IO.FileInfo(path);
            response.ContentType = "application/octet-stream";
            var result = path.Substring(path.LastIndexOf('\\') + 1);
            response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", result));
            response.AddHeader("Content-Length", fileInfo.Length.ToString()); ;
            response.WriteFile(path);
            response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}