namespace KLM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class InvalidALLVIN : DbContext
    {
        public InvalidALLVIN()
            : base("name=InvalidALLVIN")
        {
        }

        public virtual DbSet<vwALLInavildVINS> vwALLInavildVINS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
