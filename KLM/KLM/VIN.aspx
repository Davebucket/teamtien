﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="VIN.aspx.cs" Inherits="KLM.VIN" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<asp:Content runat="server" ContentPlaceHolderID="mainContent">
<%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="Content/GridEvents.js" defer></script>
   <div class="container-fluid">
        <div class="row">
            <p class="col-12 demo-content-title">Double VINS</p>
        </div>
        <div class="row">
            <div class="col-12">    
<%--                <dx:BootstrapButton runat="server" Text="PDF" ID="ButtonPDF1" CssClasses-Control="btn btn-info btn-lg">
                </dx:BootstrapButton>--%>
            <!-- Trigger the modal with a button -->
<%--            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Large Modal</button>--%>
                <dx:BootstrapGridView runat="server" ID="vinGridView" DataSourceID="EntityServerModeDataSourceVIN" KeyFieldName="rownum" OnToolbarItemClick="vinGridView_ToolbarItemClick" OnCustomCallback="vinGridView_CustomCallback" ClientInstanceName="vinGridView" OnCustomJSProperties="vinGridView_CustomJSProperties">
                            <ClientSideEvents ToolbarItemClick="onToolbarItemClick" EndCallback="onBootstrapGridViewEndCallback" Init="onGridInit" />
                            <SettingsPager AlwaysShowPager="true" PageSizeItemSettings-Visible="true" PageSize="15"></SettingsPager>
                            <Settings ShowHeaderFilterButton="true" GridLines="Horizontal" />
                            <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                            <SettingsText Title="Filtering" />
                            <SettingsDataSecurity AllowDelete="true" AllowEdit="true" AllowInsert="true" />
                            <SettingsPager EnableAdaptivity="true">
                                <Summary Position="Right" />
                                <NextPageButton IconCssClass="demo-icon demo-icon-right" Text="" />
                                <PrevPageButton IconCssClass="demo-icon demo-icon-left" Text="" />
                            </SettingsPager>
                            <CssClasses Control="demo-gridview" HeaderRow="bg-light text-dark" Row="grid-row" Table="border-top-0" />
                            <Columns>
                                <dx:BootstrapGridViewCommandColumn ShowSelectCheckbox="True" ShowClearFilterButton="true" SelectAllCheckboxMode="Page" Width="6" />
                                <dx:BootstrapGridViewDataColumn AllowTextTruncationInAdaptiveMode="true" FieldName="rownum"  Width="6" HorizontalAlign="Left" Visible="true" />
                                <dx:BootstrapGridViewDataColumn FieldName="OEM">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="Assignor">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                                <dx:BootstrapGridViewDataColumn FieldName="VIN">
                                     <SettingsHeaderFilter Mode="CheckedList" />
                                </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Country_of_purchasing__leasing_entity">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Country_of_seller___lessor">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Date_of_purchase_">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Acquisition_type">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                               <dx:BootstrapGridViewDataColumn FieldName="Procedure">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                               </dx:BootstrapGridViewDataColumn>
                            </Columns>
                            <Toolbars>
                                <dx:BootstrapGridViewToolbar ShowInsidePanel="true">
                                    <SettingsAdaptivity EnableCollapseRootItemsToIcons="true" Enabled="true" />
                                    <Items>
<%--                                        <dx:BootstrapGridViewToolbarItem Command="New">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Edit">
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="Delete">
                                        </dx:BootstrapGridViewToolbarItem>--%>
                                        <dx:BootstrapGridViewToolbarItem Text="Export" IconCssClass="demo-icon demo-icon-export">
                                            <Items>
                                                <dx:BootstrapGridViewToolbarMenuItem Command="ExportToCsv" Text="Export to Excel (XLSX)"></dx:BootstrapGridViewToolbarMenuItem>
                                                <dx:BootstrapGridViewToolbarMenuItem BeginGroup="true" Name="CustomExportToXLS" Text="Custom Export to XLS(WYSIWYG)" Command="Custom" />
                                            </Items>
                                        </dx:BootstrapGridViewToolbarItem>
                                        <dx:BootstrapGridViewToolbarItem Command="ShowCustomizationDialog">
                                        </dx:BootstrapGridViewToolbarItem>
                                    </Items>
                                </dx:BootstrapGridViewToolbar>
                            </Toolbars>
                            <SettingsExport EnableClientSideExportAPI="true" ExcelExportMode="DataAware" ExportSelectedRowsOnly="false"></SettingsExport>
                        </dx:BootstrapGridView>
            </div>
            <dx:EntityServerModeDataSource ID="EntityServerModeDataSourceVIN" ContextTypeName="KLM.DoubleVIN" TableName="DoubleVINNUmmers" OnSelecting="EntityServerModeDataSourceVIN_Selecting" runat="server" />
            <dx:BootstrapPopupMenu ID="bootstrapPopupMenu" runat="server" ClientInstanceName="bootstrapPopupMenu">
                <ClientSideEvents ItemClick="onBootstrapPopupMenuClick" />
                <Items>
                   <dx:BootstrapMenuItem Text="Search with Decisiv" IconCssClass="glyphicon glyphicon-floppy-save" Name="Decisiv" />
<%--                    <dx:BootstrapMenuItem Text="Add" IconCssClass="glyphicon glyphicon-plus" Name="Insert" />
                    <dx:BootstrapMenuItem Name="Update" IconCssClass="glyphicon glyphicon-refresh" />
                    <dx:BootstrapMenuItem Name="Delete" IconCssClass="glyphicon glyphicon-remove" />
                    <dx:BootstrapMenuItem Name="FilterRow" IconCssClass="glyphicon glyphicon-filter" />--%>
<%--                    <dx:BootstrapMenuItem Name="SearchPanel" IconCssClass="glyphicon glyphicon-search" />--%>
   <%--                 <dx:BootstrapMenuItem Name="GroupPanel" IconCssClass="glyphicon glyphicon-compressed" />--%>
<%--                    <dx:BootstrapMenuItem Name="ID" />
                    <dx:BootstrapMenuItem Name="Name" />
                    <dx:BootstrapMenuItem Name="Type" />--%>
                </Items>
            </dx:BootstrapPopupMenu>


            <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                      <p>This is a large modal.</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <dx:BootstrapPopupControl runat="server" ShowFooter="true"
                ClientInstanceName="infoPopup" PopupElementCssSelector="#info-popup-control" CloseAction="None"
                PopupHorizontalAlign="Center" PopupVerticalAlign="Middle" Width="500px">
                <HeaderTemplate>
                    <h4 class="text-primary">
                        <span class="fa fa-info-circle"></span> Information
                    </h4>
                </HeaderTemplate>
                <FooterTemplate>
                    <dx:BootstrapButton runat="server" Text="OK" AutoPostBack="false" UseSubmitBehavior="false">
                        <ClientSideEvents Click="function(s, e) { infoPopup.Hide(); }" />
                    </dx:BootstrapButton>
                </FooterTemplate>
                <ContentCollection>
                    <dx:ContentControl>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet metus vel nisi blandit tincidunt vel efficitur purus. Nunc nec turpis tempus, accumsan orci auctor, imperdiet mauris. Fusce id purus magna.</p>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:BootstrapPopupControl>
            </div>
</asp:Content>