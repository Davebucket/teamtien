namespace KLM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AirCargoData")]
    public partial class AirCargoData
    {
        [Key]
        public int RowNum { get; set; }

        public string forwardername { get; set; }

        public string country_code_origin { get; set; }

        public string country_code_dest { get; set; }

        public string y { get; set; }

        public string sourcefile { get; set; }
    }
}
