using System;
using System.Linq;

namespace KLM_Test {

    public interface IWidget {
        string Title { get; set; }
    }
}