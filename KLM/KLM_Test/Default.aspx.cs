using DevExpress.Web.Bootstrap;
using System;
using KLM_Test;
using System.DirectoryServices.AccountManagement;

namespace KLM_Test {
    public partial class Default : System.Web.UI.Page {

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            bool isMemeber = false;
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            //userName = "meijer-jo";
            PrincipalContext ctxp = new PrincipalContext(ContextType.Domain, "nl.stibbe.int", "kmadmin", "knowledge");
            // find a user
            UserPrincipal user = UserPrincipal.FindByIdentity(ctxp, userName);

            // find the group in question
            GroupPrincipal notarrgroup = GroupPrincipal.FindByIdentity(ctxp, "ASD Notary");
            // find the group in question
            GroupPrincipal notarrgroupmodellen = GroupPrincipal.FindByIdentity(ctxp, "ASD Notary Modellen");

            if (user != null && (user.IsMemberOf(notarrgroup)| user.IsMemberOf(notarrgroupmodellen)))
            {
                isMemeber = true;
            }
            */
        }

        protected void CardViewControl_CustomCallback(object sender, DevExpress.Web.ASPxCardViewCustomCallbackEventArgs e) {
            int newPageSize = Int32.Parse(e.Parameters);
            CardViewControl.SettingsPager.ItemsPerPage = newPageSize;
            CardViewControl.DataBind();
        }

        protected void BootstrapScheduler1_Init(object sender, EventArgs e) {
            var scheduler = (BootstrapScheduler)sender;
            scheduler.Storage.Appointments.Labels.Clear();
            foreach(SchedulerLabel label in SchedulerLabelsHelper.GetItems())
                scheduler.Storage.Appointments.Labels.Add(label.Id, label.Name, label.BackgroundCssClass, label.TextCssClass);
        }
    }
}